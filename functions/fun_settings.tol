
//////////////////////////////////////////////////////////////////////////////
NameBlock ReadSettings(Text xlsFile)
//////////////////////////////////////////////////////////////////////////////
{
  Set wsMain = ReadWorksheet(xlsFile, "Main");
  ObtainSettings(wsMain)
};

//////////////////////////////////////////////////////////////////////////////
NameBlock ObtainSettings(Set wsMain)
//////////////////////////////////////////////////////////////////////////////
{
  Set tbMain = Remove(Copy(wsMain), 1);
  // Se evitan problemas en la primera columna de la pesta�a Main
  // por el uso de "dataSet" en lugar de "dataset".
  Set EvalSet(tbMain, Text (Set row) { row[1] := ToLower(row[1]) });
  // Se lee la hoja principal con las configuraciones
  Set groups = Classify(tbMain, Real (Set row1, Set row2) {
    Compare(row1[1], row2[1])
  });
  Set settings0 = EvalSet(groups, NameBlock (Set group) {
    Text name = group[1][1];
    NameBlock mSettings = SetToNameBlock(EvalSet(group, Anything (Set row) {
      // Se introduce la posibilidad de utilizar variables globales #855
      If(Grammar(row[3])=="Text", {
        Text ini = TextSub(row[3], 1, 2);
        Text nme = TextSub(row[3], 3, -1);
        If(ini=="::", {
          Case(ObjectExist("Anything", row[3]), {
            Eval("Anything "<<row[2]<<" = "<<row[3])
          }, ObjectExist("Anything", nme), {
            Eval("Anything "<<row[2]<<" = "<<nme)
          }, True, {
            WriteLn("[ReadSettings] No se encuentra la variable '"
              <<row[3]<<"'.", "W");
            Eval("Text "<<row[2]<<" = row[3]")
          })
        }, {
          Eval("Text "<<row[2]<<" = row[3]")
        })
      }, {
        Eval("Anything "<<row[2]<<" = row[3]")
      })
    }));
    WithName(ToLower(name), mSettings)
  });
  Set EvalSet([["general", "dataset", "model", "estimation", "forecast"]],
    Real (Text moduleName) {
    If(Not(ObjectExist("NameBlock", "settings0::"<<moduleName)), {
      Set Append(settings0, [[WithName(moduleName, SetToNameBlock(Empty))]]);
    1}, 0)
  });
  SetToNameBlock(settings0)
};

//////////////////////////////////////////////////////////////////////////////
Anything GetSetting(NameBlock settings, Text module, Text setting, 
  Anything default)
//////////////////////////////////////////////////////////////////////////////
{
  // Se comprueba si la 'setting' se encuentra en el m�dulo solicitado,
  // si no existe se busca en general y de lo contrario se devuelve el
  // valor por defecto.
  Text lModule = ToLower(module);
  Real mSpc = ObjectExist("Anything", "settings::"<<lModule<<"::"<<setting);
  Anything settingValue = Case(mSpc, {
    Eval("settings::"<<lModule<<"::"<<setting)
  }, ObjectExist("Anything", "settings::general::"<<setting), {
    Eval("settings::general::"<<setting)
  }, True, default);
  // Se consideran algunos casos particulares:
  //  * [name] Si no es espec�fico del m�dulo (mSpc==1) se a�ade un prefijo
  //  * [version] Se devuelve siempre como texto (#908)
  Case(setting=="name", {
    Text prefix = If(mSpc, "", FirstToUpper(TextSub(lModule, 1, 3))<<".");
    Text prefix<<settingValue
  }, setting=="version", {
    Text ""<<settingValue
  }, True, settingValue)
};

//////////////////////////////////////////////////////////////////////////////
